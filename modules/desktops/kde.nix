{ lib, config, pkgs, ... }:
let
  inherit (lib) mkIf mkEnableOption;
  cfg = config.desktops.kde;
in
{
  options.desktops.kde = {
    enable = mkEnableOption "Plasma Desktop Environment";
  };

  config = mkIf cfg.enable {

    services.xserver.desktopManager.plasma5.enable = true;

    environment.systemPackages = with pkgs; [
      ark
      libsForQt5.okular
      kdeconnect
    ];
  };

}
