{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.desktops.gnome;
  inherit (lib) mkIf mkEnableOption;
in {
  options.desktops.gnome = {
    enable = mkEnableOption "GNOME Desktop Environment";
  };

  config = mkIf cfg.enable {
    services.xserver.desktopManager.gnome.enable = true;
    services.gnome.gnome-keyring.enable = true;

    services.gnome.core-utilities.enable = false;
    environment.gnome.excludePackages = [pkgs.orca];

    environment.systemPackages = with pkgs; [
      gnomeExtensions.user-themes
      gnomeExtensions.tray-icons-reloaded
      gnome.gnome-tweaks
      gnome.dconf-editor
      gnome.gnome-system-monitor
      gnome.eog
      gnome.gnome-clocks
      gnome.gnome-calculator
      gnome.gnome-software
      gnomeExtensions.just-perfection
      gnomeExtensions.gsconnect
      gnomeExtensions.caffeine
      gnomeExtensions.blur-my-shell
      gnome-secrets
      gnomeExtensions.tactile
    ];
  };
}
