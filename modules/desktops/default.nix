{
  pkgs,
  config,
  nixpkgs-wayland,
  lib,
  ...
}: let
  cfg = config.desktops;
  inherit (lib) mkIf mkEnableOption;
  wayland-pkgs = nixpkgs-wayland.packages."x86_64-linux";
in {
  imports = [
    ./i3.nix
    ./wayfire.nix
    ./sway.nix
    ./gnome.nix
    ./awesome.nix
    ./xfce.nix
    ./kde.nix
    ./applications.nix
  ];

  options.desktops = {enable = mkEnableOption "Desktop";};

  config = mkIf cfg.enable {
    services.xserver.enable = true;
    services.xserver.libinput.enable = true;
    services.xserver.displayManager.gdm.enable = true;
    services.xserver.xkbOptions = "ctrl:nocaps";

    security.polkit.enable = true;
    services.input-remapper.enable = true;


    services.flatpak.enable = true;
    hardware.acpilight.enable = true;
    services.gnome.gnome-keyring.enable = true;
    security.pam.services.login.enableGnomeKeyring = true;
    services.dbus.enable = true;
    services.udev.extraRules = ''
      ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", MODE="0666", RUN+="${pkgs.coreutils}/bin/chmod a+w /sys/class/backlight/%k/brightness"
    '';

    # XDG Config
    xdg.portal = {
      enable = true;
      wlr.enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr
        xdg-desktop-portal-gnome
      ];
    };

    # Pipewire
    sound.enable = false;
    security.rtkit.enable = true;
    hardware.pulseaudio.enable = false;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      jack.enable = true;
      pulse.enable = true;
      audio.enable = true;
      media-session.enable = true;
      wireplumber.enable = false;
    };

    # OpenGL
    hardware.opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      extraPackages32 = with pkgs.pkgsi686Linux;
        [libva vaapiIntel libvdpau-va-gl vaapiVdpau]
        ++ lib.optionals config.services.pipewire.enable [pipewire];
    };


  fonts = {
    fontDir.enable = true;
    fonts = with pkgs; [
      powerline-fonts
      font-awesome_4
      material-icons
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-cjk-serif
      twemoji-color-font
      noto-fonts-emoji-blob-bin
      emojione
      hack-font
      (nerdfonts.override {
        fonts = ["Hack" "UbuntuMono" "DejaVuSansMono"];
      })
    ];
  };
  };
}
