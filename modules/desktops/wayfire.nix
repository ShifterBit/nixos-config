{ lib, config, pkgs, nixpkgs-wayland, mypkgs, inputs, ... }:
let
  cfg = config.desktops.wayfire;
  inherit (lib) mkIf mkEnableOption;
in
{
  options.desktops.wayfire = {
    enable = mkEnableOption "Wayfire Window Manager";
  };
  config = mkIf cfg.enable
    {
      services.logind.extraConfig = ''
        HandlePowerKey=suspend
      '';
      environment.systemPackages = with pkgs; [
        swayidle
        mypkgs.waybar
        xwayland
        wl-clipboard
        wlogout
        pkgs.swaynotificationcenter
        wmctrl
        clipman
        wcm
        swaylock-fancy
        xsettingsd
        wofi
        light
        wdisplays
        mypkgs.dmenu
        xwayland
        sway-contrib.grimshot
        acpi
        dex
        python310Packages.dasbus
        brightnessctl
        qt5ct
      ];

      services.xserver.displayManager.session = [
        {
          manage = "window";
          name = "Wayfire";
          start = "${mypkgs.wayfire}/bin/wayfire";
        }
      ];


    };
}
