{
  lib,
  config,
  pkgs,
  mypkgs,
  mpkgs,
  ...
}: let
  inherit (lib) mkIf mkEnableOption;
  cfg = config.gaming;
in {
  options.gaming = {
    enable = mkEnableOption "Some Gaming stuff";
  };

  config = mkIf cfg.enable {
    services.joycond.enable = true;
    hardware.xone.enable = true;
    environment.systemPackages = [pkgs.sc-controller];

    programs.gamemode.enable = true;
    hardware.steam-hardware.enable = true;
    services.udev.packages = [mypkgs.game-devices-udev];
    hardware.uinput.enable = true;
  };
}
