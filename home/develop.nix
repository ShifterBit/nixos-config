{
  pkgs,
  mypkgs,
  inputs,
  ...
}: {
  home.packages = with pkgs; [
    # Language Servers
    nodePackages.pyright
    nodePackages.typescript-language-server
    nodePackages.vscode-langservers-extracted
    sumneko-lua-language-server
    rust-analyzer
    inputs.nil-lsp.packages.x86_64-linux.nil
    gopls
    gotools

    # Linters/Formatters
    nixpkgs-fmt
    ormolu
    shellcheck
    black
    stylua
    statix
    mypkgs.alejandra
    yamllint

    # Debuggers
    gdb
    delve

    # etc
    watchexec
    cht-sh
    awscli2
    ngrok
    sqlite
    tabnine
    zeal
    exercism
    distrobox
    lazygit
  ];
}
