{
  vimUtils,
  fetchFromGitHub,
}:
vimUtils.buildVimPlugin {
  namePrefix = "";
  name = "telescope-manix";
  src = fetchFromGitHub {
    owner = "MrcJkb";
    repo = "telescope-manix";
    rev = "9f91408328be8719ca1f6c49a3ccc1df6392d5e9";
    sha256 = "sha256-Yn4H4qVXOZYcluwCfivj7SIxroNqjEbaEom6gXl9kw8=";
  };
}
