{
  vimUtils,
  fetchFromGitHub,
  pkgs,
}:
vimUtils.buildVimPlugin {
  name = "go.nvim";
  namePrefix = "";
  src = fetchFromGitHub {
    owner = "ray-x";
    repo = "go.nvim";
    rev = "825f20b0366302f0d587b2a1280d4690fe2bbc27";
    sha256 = "sha256-eoO/qVuuYAZPnQjNodXyoYYDTaFRNCMIL5ciimWYTQw=";
  };
  buildPhase = "rm Makefile";
}
