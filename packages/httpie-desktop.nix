{
  appimageTools,
  makeDesktopItem,
  stdenvNoCC,
}: let
  httpie-desktop = appimageTools.wrapType2 {
    name = "httpie-desktop";
    src = builtins.fetchurl {
      url = "https://github.com/httpie/desktop/releases/download/v2022.13.0/HTTPie-2022.13.0.AppImage";
      sha256 = "17l3xiwvl8pk2kb4gp90ivjyxf926indf6gb4b8m8agzwx2d3s4s";
    };
  };

  desktopItem = makeDesktopItem {
    name = "httpie-desktop";
    desktopName = "HTTPie Desktop";
    comment = "RSS Feed Reader";
    exec = "${httpie-desktop}/bin/httpie-desktop %u";
  };
in
  stdenvNoCC.mkDerivation {
    pname = "httpie-desktop";
    version = "2022.13.0";
    src = httpie-desktop;
    installPhase = ''
      mkdir -p $out/bin $out/share/applications
      install -m 755 $src/bin/httpie-desktop $out/bin
      install -m 444 -D ${desktopItem}/share/applications/* -t $out/share/applications
    '';
  }
