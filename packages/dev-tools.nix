{
  appimageTools,
  makeDesktopItem,
  stdenvNoCC,
  libthai,
}: let
  dev-tools = appimageTools.wrapType2 {
    name = "dev-tools";
    src = builtins.fetchurl {
      url = "https://github.com/fosslife/devtools-x/releases/download/DevToolsv1.7.0/dev-tools_1.7.0_amd64.AppImage";
      sha256 = "1yjqg77dnr3n1npj8i7l4x7m7lq355l4mbik2dl6a97dr3jjxsfs";
    };
    extraPkgs = pkgs: [libthai];
  };

  desktopItem = makeDesktopItem {
    name = "dev-tools";
    desktopName = "Dev Tools";
    exec = "${dev-tools}/bin/dev-tools %u";
  };
in
  stdenvNoCC.mkDerivation {
    pname = "httpie-desktop";
    version = "2022.13.0";
    src = dev-tools;

    installPhase = ''
      mkdir -p $out/bin $out/share/applications
      install -m 755 $src/bin/dev-tools $out/bin
      install -m 444 -D ${desktopItem}/share/applications/* -t $out/share/applications
    '';
  }
