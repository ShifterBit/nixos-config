{
  stdenv,
  fetchFromGitHub,
  meson,
  ninja,
  gtk3,
  sassc,
}:
stdenv.mkDerivation {
  name = "adw-gtk3";
  src = fetchFromGitHub {
    owner = "lassekongo83";
    repo = "adw-gtk3";
    rev = "4d4a36f0695a141bdea6dbf634cbe56bb4c0724d";
    sha256 = "sha256-yTzVBrnH/rfWzHHT/jr+p85ZO42kPMS/7qZyUHS2YIM=";
  };
  buildInputs = [meson ninja gtk3 sassc];
}
