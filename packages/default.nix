{
  self,
  inputs,
  ...
}: let
  system = "x86_64-linux";
  pkgs = inputs.nixpkgs.legacyPackages.${system};
  wpkgs = inputs.nixpkgs-wayland.packages.${system};
in {
  dmenu = pkgs.callPackage ./dmenu.nix {};
  dmenu-wayland = pkgs.callPackage ./dmenu-wayland.nix {};
  adw-gtk3 = pkgs.callPackage ./adw-gtk3.nix {};
  alejandra = inputs.alejandra.defaultPackage.${system};
  game-devices-udev = pkgs.callPackage ./game-devices-udev.nix {};
  fluent-reader = pkgs.callPackage ./fluent-reader.nix {};
  httpie-desktop = pkgs.callPackage ./httpie-desktop.nix {};
  dev-tools = pkgs.callPackage ./dev-tools.nix {};
  cinny-desktop = pkgs.callPackage ./cinny-desktop.nix {};
  nvim-cmp-nixpkgs = pkgs.callPackage ./nvim-cmp-nixpkgs.nix {};
  dracula-nvim = pkgs.callPackage ./dracula-nvim.nix {};
  vim-livedown = pkgs.callPackage ./vim-livedown.nix {};
  aws-shell = pkgs.callPackage ./aws-shell.nix {};
  nvim-telescope-manix = pkgs.callPackage ./nvim-telescope-manix.nix {};
  go-nvim = pkgs.callPackage ./go-nvim.nix {};
  nvim-dap-go = pkgs.callPackage ./nvim-dap-go.nix {};
  nvim-treesitter-textsubjects = pkgs.callPackage ./nvim-treesitter-textsubjects.nix {};
  inherit (inputs.nix-software-center.packages.${system}) nix-software-center;
}
