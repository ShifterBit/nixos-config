{
  pkgs,
  stdenv,
  fetchFromGitLab,
  ...
}:
stdenv.mkDerivation {
  name = "game-devices-udev";
  src = fetchFromGitLab {
    owner = "fabiscafe";
    repo = "game-devices-udev";
    rev = "cfa51979d51cbb018235ed351d6e9eff38b236db";
    sha256 = "sha256-Yy91yDF5BSDTTlr/Pj8e0UklPooEdzvRW8mkhdHtHVo=";
  };
  dontBuild = true;
  dontConfigure = true;
  installPhase = ''
    mkdir -p $out/etc/udev/rules.d
    cp $src/*.rules $out/etc/udev/rules.d
    sed -i s#/bin/sh#${pkgs.dash}/bin/dash#g $out/etc/udev/rules.d/71-powera-controllers.rules
    sed -i s#udevadm#${pkgs.systemd}/bin/udevadm#g $out/etc/udev/rules.d/71-powera-controllers.rules
  '';
}
